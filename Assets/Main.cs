using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Math = System.Math;

public class Main : MonoBehaviour
{
    public static Main main;

    class HexTilePtr : MonoBehaviour
    {
        public HexTile tile;

        // Update is called once per frame
        void Update ()
        {
            tile.UpdateRotation();
        }
    }
    class HexTile
    {
        Vector3 position;
        public GameObject visual;
        public int rot_offset;
        GameObject[] spikes = new GameObject [6];
        public GameObject spike_at (int dir)
        {
            return spikes[(dir + 6 - rot_offset) % 6];
        }

        static GameObject HexTilePrefab = (GameObject)Resources.Load("hextile", typeof(GameObject));

        public HexTile (Vector3 position)
        {
            //Debug.Log("Spawning hex tile at " + position);
            this.position = position;
            visual = Instantiate(HexTilePrefab, position, Quaternion.identity);

            HexTilePtr ptr = visual.AddComponent(typeof(HexTilePtr)) as HexTilePtr;
            ptr.tile = this;

            for (int i = 0; i < 6; i++)
            {
                //Debug.Log("visual: "+visual);
                spikes[i] = visual.transform.Find("arm"+i).gameObject;
            }
        }
        public void SpikeDelete (bool[] deletes)
        {
            bool del_main = true;
            for (int i = 0; i < 6; i++)
            {
                if (deletes[i]) del_main = false;
                else
                {
                    Destroy(spikes[i]);
                    spikes[i] = null;
                }
            }
            if (del_main) Destroy(visual);
        }

        bool animating = false;
        bool anim_back = false;
        double anim_progress;
        double rot_time = 0.2;
        public void UpdateRotation ()
        {
            if (animating)
            {
                anim_progress += Time.deltaTime / rot_time;
                double rot_now = rot_offset * 60;
                if (anim_progress >= 1) animating = false;
                else
                {
                    double back = (1 - anim_progress) * 60;
                    if (anim_back) rot_now += back;
                    else rot_now -= back;
                }

                visual.transform.rotation = Quaternion.identity;
                visual.transform.Rotate(0,(float)rot_now,0);
            }
        }
        public void RotateRight ()
        {
            if (animating)
            {
                if (!anim_back) anim_progress -= 1;
                else
                {
                    anim_progress += 1;
                    if (anim_progress >= 1)
                    {
                        anim_progress = 1 - (anim_progress - 1);
                        anim_back = false;
                    }
                }
            }
            else
            {
                animating = true;
                anim_progress = 0;
                anim_back = false;
            }
            rot_offset = (rot_offset + 1) % 6;
            main.CheckWin();
        }
        public void RotateLeft ()
        {
            if (animating)
            {
                if (anim_back) anim_progress -= 1;
                else
                {
                    anim_progress += 1;
                    if (anim_progress >= 1)
                    {
                        anim_progress = 1 - (anim_progress - 1);
                        anim_back = true;
                    }
                }
            }
            else
            {
                animating = true;
                anim_progress = 0;
                anim_back = true;
            }
            rot_offset = (rot_offset + 6 - 1) % 6;
            main.CheckWin();
        }
    }


    HexTile[] grid;

    static bool RandBool ()
    {
        int chance_numer = 5,
            chance_denom = 10;
        return Random.Range(0,chance_denom) < chance_numer;
    }

    Material SpikeAloneMat;
    Material SpikeHappyMat;
    public void SetSatisfied (GameObject spike, bool satisfied)
    {
        spike.GetComponent<MeshRenderer>().material = satisfied ? SpikeHappyMat : SpikeAloneMat;
    }

    int MetaHexDegree = 2;
    int GridPitch;
    Light main_light;
    SpriteRenderer white_square;
    // Start is called before the first frame update
    void Start ()
    {
        SpikeAloneMat = (Material)Resources.Load("SpikeAlone", typeof(Material));
        SpikeHappyMat = (Material)Resources.Load("SpikeHappy", typeof(Material));
        GameObject main_light_obj = GameObject.Find("Directional Light");
        main_light = main_light_obj.GetComponent<Light>();
        GameObject white_square_obj = GameObject.Find("White Square");
        //white_square_obj.transform.localScale = new Vector3(float.PositiveInfinity,float.PositiveInfinity,0);
        white_square = white_square_obj.GetComponent<SpriteRenderer>();
        main = this;

        GridPitch = MetaHexDegree * 2 - 1;
        float ScreenFillProportion = 0.8f;
        Camera.main.orthographicSize = GridPitch / ScreenFillProportion;
        float square_size = Camera.main.orthographicSize * 2f * 100;
        white_square_obj.transform.localScale = new Vector3(square_size,square_size,0);
        float row_height = -Mathf.Sin((60)*Mathf.PI/180);
        float global_minus = (MetaHexDegree - 1);
        Vector3 global_minus_vec = new Vector3(global_minus * 2,0,global_minus * row_height * 2);
        grid = new HexTile [GridPitch * GridPitch];
        bool[] ConnsRight = new bool [GridPitch * GridPitch];
        bool[] ConnsDownLeft = new bool [GridPitch * GridPitch];
        bool[] ConnsDownRight = new bool [GridPitch * GridPitch];
        Vector3 pos = new Vector3(0,0,0);
        for (int y = 0; y < GridPitch; y++)
        {
            int post_mid = (y + 1) - MetaHexDegree;
            int from_mid = Math.Abs(post_mid);
            int row_width = GridPitch - from_mid;
            float leftpos = from_mid * 0.5f;
            int conn_idx = y * GridPitch;

            for (int x = 0; x < row_width; x++, conn_idx++)
            {
                pos.x = leftpos + x;
                pos.z = y * row_height;
                HexTile tile = new HexTile(pos * 2 - global_minus_vec);
                grid[y * GridPitch + x] = tile;

                bool xnext = x + 1 < row_width;
                bool ynext = y + 1 < GridPitch;
                if (xnext) ConnsRight[conn_idx] = RandBool();
                if (ynext && (x > 0 || post_mid < 0)) ConnsDownLeft[conn_idx] = RandBool();
                if (ynext && (xnext || post_mid < 0)) ConnsDownRight[conn_idx] = RandBool();

                bool upleft = false,
                     upright = false,
                     left = false,
                     right = false,
                     downleft = false,
                     downright = false;

                int above_x_add = post_mid > 0 ? 1 : 0;

                if (x > 0) left = ConnsRight[conn_idx - 1];
                if (y > 0 && (x > 0 || post_mid > 0)) upleft = ConnsDownRight[conn_idx - GridPitch - 1 + above_x_add];
                if (y > 0) upright = ConnsDownLeft[conn_idx - GridPitch + above_x_add];
                right = ConnsRight[conn_idx];
                downleft = ConnsDownLeft[conn_idx];
                downright = ConnsDownRight[conn_idx];

                bool[] spike_deletes = {upright,right,downright,downleft,left,upleft};
                tile.SpikeDelete(spike_deletes);

                // Randomize tile rotations so user must solve the puzzle.
                tile.rot_offset = Random.Range(0,6);
                tile.visual.transform.Rotate(0, tile.rot_offset * 60, 0, Space.Self);
            }
        }
        CheckWin();
    }
    void Clear ()
    {
        for (int i = 0; i < grid.Length; i++)
        {
            HexTile tile = grid[i];
            if (tile != null)
                GameObject.Destroy(tile.visual);
        }
    }
    bool WinState = false;
    double WinElapsed;
    Color WinColor;
    void CheckWin ()
    {
        bool solved = true;
        for (int y = 0; y < GridPitch; y++)
        {
            int post_mid = (y + 1) - MetaHexDegree;
            int from_mid = Math.Abs(post_mid);
            int row_width = GridPitch - from_mid;
            int conn_idx = y * GridPitch;

            for (int x = 0; x < row_width; x++, conn_idx++)
            {
                HexTile tile = grid[y * GridPitch + x];

                GameObject inward_upright = null,
                           inward_right = null,
                           inward_downright = null,
                           inward_downleft = null,
                           inward_left = null,
                           inward_upleft = null;

                int above_x_add = post_mid > 0 ? 1 : 0;
                int below_x_minus = post_mid >= 0 ? 1 : 0;
                bool xnext = x + 1 < row_width;
                bool ynext = y + 1 < GridPitch;
                if (x > 0) inward_left = grid[conn_idx - 1].spike_at(1);
                if (xnext) inward_right = grid[conn_idx + 1].spike_at(4);
                if (y > 0)
                {
                    if (x > 0 || post_mid > 0) inward_upleft = grid[conn_idx - GridPitch - 1 + above_x_add].spike_at(2);
                    if (xnext || post_mid > 0) inward_upright = grid[conn_idx - GridPitch + above_x_add].spike_at(3);
                }
                if (ynext)
                {
                    if (x > 0 || post_mid < 0) inward_downleft = grid[conn_idx + GridPitch - below_x_minus].spike_at(0);
                    if (xnext || post_mid < 0) inward_downright = grid[conn_idx + GridPitch + 1 - below_x_minus].spike_at(5);
                }

                GameObject[] spikes_inward = {inward_upright,inward_right,inward_downright,inward_downleft,inward_left,inward_upleft};

                for (int i = 0; i < 6; i++)
                {
                    GameObject inward = spikes_inward[i];
                    GameObject outward = tile.spike_at(i);
                    if (outward != null)
                    {
                        if (inward == null) solved = false;
                        SetSatisfied(outward, inward != null);
                    }
                }
            }
        }

        if (solved)
        {
            WinState = true;
            WinElapsed = 0;

            float back_hue = Random.Range(0f,1f);
            WinColor = Color.HSVToRGB(back_hue,0.75f,0.75f);
        }
    }

    HexTile GetClickedTile ()
    {
        Ray ray = GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit)) // See if we clicked a game object...
        {
            // Safe to assume the clicked object was part of a hextile prefab,
            // since there are no other objects in the game that we'd possibly
            // have clicked on since nothing else exists.
            GameObject visual = hit.transform.parent.gameObject;
            HexTilePtr ptr = visual.GetComponent<HexTilePtr>() as HexTilePtr;
            return ptr.tile;
        }
        return null;
    }

    bool FadingToWhite = false;
    double FadeElapsed;
    bool FadingFromWhite = false;

    // Update is called once per frame
    void Update ()
    {
        FadeElapsed += Time.deltaTime;
        if (FadingFromWhite)
        {
            if (FadeElapsed >= 1)
            {
                FadingFromWhite = false;
                FadeElapsed = 1;
            }
            white_square.color = new Color(1,1,1,1 - (float)FadeElapsed);
        }

        if (WinState)
        {
            WinElapsed += Time.deltaTime;
            if (WinElapsed >= 1) WinElapsed = 1;
            Camera.main.backgroundColor = Color.Lerp(Color.black,WinColor,(float)WinElapsed);
            main_light.color = Color.Lerp(Color.white,WinColor * 0.5f,(float)WinElapsed);

            if (FadingToWhite)
            {
                if (FadeElapsed >= 1)
                {
                    WinState = false;

                    Camera.main.backgroundColor = new Color(0,0,0);
                    main_light.color = new Color(1,1,1);
                    Clear();

                    MetaHexDegree++;
                    Start();

                    FadingToWhite = false;
                    FadingFromWhite = true;
                    FadeElapsed = 0;
                    white_square.color = new Color(1,1,1,1);
                }
                else white_square.color = new Color(1,1,1,(float)FadeElapsed);
            }
            else
            {
                if (Input.GetMouseButtonDown(0))
                {
                    FadingToWhite = true;
                    FadeElapsed = 0;
                }
            }
        }
        else
        {
    		if (Input.GetMouseButtonDown(0)) // If left button pressed...
    		{
                HexTile tile = GetClickedTile();
                if (tile != null) tile.RotateRight();
    	    }
    		if (Input.GetMouseButtonDown(1)) // If right button pressed...
    		{
                HexTile tile = GetClickedTile();
                if (tile != null) tile.RotateLeft();
    	    }
        }
    }
}
